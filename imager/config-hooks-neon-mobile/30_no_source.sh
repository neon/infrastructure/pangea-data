#!/bin/sh
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex
echo "config-hooks-neon -- 30-no-source"

echo 'LB_SOURCE=false' > config/source
