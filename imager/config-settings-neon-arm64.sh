set -ex
echo "neon -- config-settings-neon-plasma-desktop-arm64"

EDITION=$(echo $NEONARCHIVE | sed 's,/, ,')

## set the various publishing deets
export LB_ISO_APPLICATION="KDE neon Plasma Desktop arm64 Live"
export LB_ISO_PREPARER="KDE neon team"
export LB_ISO_PUBLISHER="KDE neon team"
export LB_ISO_VOLUME="${IMAGENAME} ${EDITION} Mobile \$(date +%Y%m%d)"

## other various other live-build settings
export LB_APT_SOURCE_ARCHIVES="true"
export LB_LINUX_PACKAGES="linux"
#export LB_GRUB_SPLASH="breeze"

## set our kernel prefs
if [ "$VERSION_CODENAME" = "jammy" ]; then
    export LB_LINUX_FLAVOURS="generic-hwe-22.04"
else
    export LB_LINUX_FLAVOURS="generic-hwe-24.04"
fi

## convenience settings
export _COLOR="true"
export _DEBUG="true"
#export _VERBOSE="true"
