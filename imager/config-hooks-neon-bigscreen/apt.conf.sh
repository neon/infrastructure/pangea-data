#!/bin/sh
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex
echo "config-hooks-neon -- **-apt.conf"

rm config/chroot_apt/apt.conf || true
echo 'Debug::pkgProblemResolver "true";' >> config/chroot_apt/apt.conf
echo 'Acquire::Languages "none";' >> config/chroot_apt/apt.conf
