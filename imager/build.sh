#!/bin/sh

set -ex
echo "neon -- starting build.sh"

cleanup() {
    if [ ! -d build ]; then
        mkdir build
    fi
    if [ ! -d result ]; then
        mkdir result
    fi
    rm -rf $WD/result/*
    rm -rf $WD/build/livecd.ubuntu.*
    rm -rf $WD/build/source.debian*
}

export WD=$1
export DIST=$2
export ARCH=$3
export TYPE=$4
export METAPACKAGE=$5
export IMAGENAME=$6
export NEONARCHIVE=$7
export FUTURE_SERIES=$8

if [ -z $WD ] || [ -z $DIST ] || [ -z $ARCH ] || [ -z $TYPE ] || [ -z $METAPACKAGE ] || [ -z $IMAGENAME ] || [ -z $NEONARCHIVE ] [ -z $FUTURE_SERIES ]; then
    echo "!!! Not all arguments provided! ABORT !!!"
    env
    exit 1
fi

cat /proc/self/cgroup

hostname

uname --all

# FIXME: let nci/lib/setup_repo.rb handle the repo setup as well this is just
# duplicate code here...
ls -lah /tooling/nci
/tooling/nci/setup_apt_repo.rb --no-repo
sudo apt-add-repository http://archive.neon.kde.org/${NEONARCHIVE}
sudo apt update
sudo apt dist-upgrade -y
sudo apt install -y --no-install-recommends \
    git ubuntu-defaults-builder wget ca-certificates zsync distro-info \
    livecd-rootfs xorriso base-files lsb-release neon-settings neon-keyring debootstrap
if [ $ARCH = 'amd64' ]; then
    sudo apt install -y --no-install-recommends syslinux-utils cd-boot-images-amd64
fi
if [ $ARCH = 'arm64' ]; then
    sudo apt install -y --no-install-recommends cd-boot-images-arm64
fi
cd $WD
ls -lah
cleanup
ls -lah

# hicky hack for focal+
# debootstrap is kinda container aware now and also very broken.
# The debian-common script (and by extension the ubuntus) link the container
# /proc into place when detecting docker (similar to how fakeroot would work)
# but then ALSO attempt to setup a proc from scratch (as would be the case
# without fakeroot). This then smashes the container's /proc and everything
# goes up in flames. To prevent this we force-disable the container logic.
# Our ISO containers are specifically twiddled so they behave like an ordinary
# chroot and so the "native" proc logic will work just fine.
# Half using container logic and half not, however, is not fine.
# Simply prepend CONTAINER="" to unset whatever detect_container() set.
# NB: all ubuntus are symlinked to gutsy, so that's why we edit gutsy.
echo 'CONTAINER=""' > /usr/share/debootstrap/scripts/gutsy.new
cat /usr/share/debootstrap/scripts/gutsy >> /usr/share/debootstrap/scripts/gutsy.new
mv /usr/share/debootstrap/scripts/gutsy.new /usr/share/debootstrap/scripts/gutsy

cd $WD/build

sed -i \
    's%SEEDMIRROR=https://metadata.neon.kde.org/germinate/seeds/%SEEDMIRROR=https://metadata.neon.kde.org/germinate/seeds%g' \
    /usr/share/livecd-rootfs/live-build/auto/config

_DATE=$(date +%Y%m%d)
_TIME=$(date +%H%M)
DATETIME="${_DATE}-${_TIME}"
DATE="${_DATE}${_TIME}"

# Random nonesense sponsored by Rohan.
# Somewhere in utopic things fell to shit, so lb doesn't pack all files necessary
# for isolinux on the ISO. Why it happens or how or what is unknown. However linking
# the required files into place seems to solve the problem. LOL.
# sudo ln -s /usr/lib/syslinux/modules/bios/ldlinux.c32 /usr/share/syslinux/themes/ubuntu-$DIST/isolinux-live/ldlinux.c32
# sudo ln -s /usr/lib/syslinux/modules/bios/libutil.c32 /usr/share/syslinux/themes/ubuntu-$DIST/isolinux-live/libutil.c32
# sudo ln -s /usr/lib/syslinux/modules/bios/libcom32.c32 /usr/share/syslinux/themes/ubuntu-$DIST/isolinux-live/libcom32.c32

# # Compress with XZ, because it is awesome!
# JOB_COUNT=2
# export MKSQUASHFS_OPTIONS="-comp xz -processors $JOB_COUNT"

# Since we can not define live-build options directly, let's cheat our way
# around defaults-image by exporting the vars lb uses :O

## Super internal var used in lb_binary_disk to figure out the version of LB_DISTRIBUTION
# used in e.g. renaming the ubiquity .desktop file on Desktop by casper which gets it from
# /cdrom/.disk/info from live-build lb_binary_disk
EDITION=$TYPE
export RELEASE_${DIST}=${EDITION}

## Bring down the overall size a bit by using a more sophisticated albeit expensive algorithm.
export LB_COMPRESSION=none

## Create a zsync file allowing over-http delta-downloads.
export LB_ZSYNC=true # This is overridden by silly old defaults-image...

## Use our cache as proxy.
# FIXME: get out of nci/lib/setup_repo.rb
export LB_APT_HTTP_PROXY="http://apt.cache.pangea.pub:8000"

## Also set the proxy on apt options. This is used internally to expand on a lot
## of apt-get calls. For us primarily of interest because it is used for
## lb_source, which would otherwise bypass the proxy entirely.
export APT_OPTIONS="--yes -o Acquire::http::Proxy='$LB_APT_HTTP_PROXY'"

## see if we can (ab)use LB_ARCHIVES to help with problematic neon.sources getting to the chroot
export LB_ARCHIVES="http://archive.neon.kde.org/${NEONARCHIVE}"

[ -z "$CONFIG_SETTINGS" ] && CONFIG_SETTINGS="$(dirname "$0")/config-settings-${IMAGENAME}.sh"
[ -z "$CONFIG_HOOKS" ] && CONFIG_HOOKS="$(dirname "$0")/config-hooks-${IMAGENAME}"
[ -z "$BUILD_HOOKS" ] && BUILD_HOOKS="$(dirname "$0")/build-hooks-${IMAGENAME}"
[ -z "$SEEDED_SNAPS" ] && SEEDED_SNAPS="$(dirname "$0")/seeded-snaps-${IMAGENAME}"

# direct to correct config and hook locations
if [ $TYPE = 'developer' ] || [ $TYPE = 'ko' ] || [ $TYPE = 'mobile' ] || [ $TYPE = 'bigscreen' ]; then
    CONFIG_SETTINGS="$(dirname "$0")/config-settings-${IMAGENAME}-${TYPE}.sh"
    CONFIG_HOOKS="$(dirname "$0")/config-hooks-${IMAGENAME}-${TYPE}"
    BUILD_HOOKS="$(dirname "$0")/build-hooks-${IMAGENAME}-${TYPE}"
fi

export CONFIG_SETTINGS CONFIG_HOOKS BUILD_HOOKS SEEDED_SNAPS

# Preserve envrionment -E plz.
sudo -E $(dirname "$0")/ubuntu-defaults-image \
    --package $METAPACKAGE \
    --arch $ARCH \
    --release $DIST \
    --flavor neon \
    --components main,restricted,universe,multiverse

# build is finished
cat config/common

# debug the result
ls -lah

# fail is appropriate
if [ ! -e livecd.neon.iso ]; then
    echo "ISO Build Failed."
    ls -la
    cleanup
    exit 1
fi

# mv the booty
mv livecd.neon.* ../result/
cd ../result/



# All *release* iso's and also during LTS update all FUTURE_SERIES iso are published to a different location
# and naming altered to include $DIST to be able to differentiate iso's e.g.-> neon-noble-release-#######.iso
# we also override TYPE with the 'new_type' variable to alter naming for non-desktop editions
new_type="${NEONARCHIVE}-${TYPE}"

if [ $TYPE = 'release' ] || [ $DIST == "${FUTURE_SERIES}" ]; then
    if [ $TYPE = 'bigscreen' ] || [ $TYPE = 'developer' ] || [ $TYPE = 'ko' ] ||  [ $TYPE = 'mobile' ]; then
        new_iso_name_timestamped="${IMAGENAME}-${DIST}-$new_type-${DATETIME}"
    else
        new_iso_name_timestamped="${IMAGENAME}-${DIST}-${TYPE}-${DATETIME}"
    fi
elif [ $TYPE = 'bigscreen' ] || [ $TYPE = 'developer' ] || [ $TYPE = 'ko' ] ||  [ $TYPE = 'mobile' ]; then
    new_iso_name_timestamped="${IMAGENAME}-$new_type-${DATETIME}"
else
    # don't change desktop iso's names for current_release
    new_iso_name_timestamped="${IMAGENAME}-${TYPE}-${DATETIME}"
fi

# timestamp our artifacts
for f in live*; do
    new_iso_name_timestamper=$(echo $f | sed "s/livecd\.neon/$new_iso_name_timestamped/")
    mv $f $new_iso_name_timestamper
done

if [ $TYPE = 'release' ] || [ $DIST == "${FUTURE_SERIES}" ]; then
    if [ $TYPE = 'bigscreen' ] || [ $TYPE = 'developer' ] || [ $TYPE = 'ko' ] ||  [ $TYPE = 'mobile' ]; then
        new_iso_name_current="${IMAGENAME}-${DIST}-$new_type-current"
    else
        new_iso_name_current="${IMAGENAME}-${DIST}-${TYPE}-current"
    fi
elif [ $TYPE = 'bigscreen' ] || [ $TYPE = 'developer' ] || [ $TYPE = 'ko' ] ||  [ $TYPE = 'mobile' ]; then
    new_iso_name_current="${IMAGENAME}-$new_type-current"
else
    new_iso_name_current="${IMAGENAME}-${TYPE}-current"
fi

ln -s $new_iso_name_timestamped.iso $new_iso_name_current.iso
zsyncmake $new_iso_name_current.iso
sha256sum $new_iso_name_timestamped.iso > $new_iso_name_timestamped.sha256sum

# write our torrent message
cat > .message << END
KDE neon

$new_iso_name_timestamped.iso Live and Installable ISO
$new_iso_name_timestamped.iso.sig PGP Digital Signature
$new_iso_name_timestamped.manifest ISO contents
$new_iso_name_timestamped.sha256sum Checksum
$new_iso_name_timestamped.torrent Web Seed torrent (you client needs to support web seeds or it may not work)
"current" files are the same files for those wanting a URL which does not change daily.
END

# change ownership to make jenkins happy and pass through required bits
echo $DATETIME > date_stamp
echo $new_type > new_type
pwd
chown -Rv jenkins:jenkins .

exit 0
