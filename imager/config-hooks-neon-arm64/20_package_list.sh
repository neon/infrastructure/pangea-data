#!/bin/sh
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex
echo "config-hooks-neon -- 20-package-list"

# konsole needs installed first else xterm gets installed cos xorg deps on
# terminal | xterm and doesn't know terminal is installed later in the tree.
# Also explicitly install the efi image packages explicitly so live-build
# can find them for extraction into the ISO.
cat << EOF > config/package-lists/ubuntu-defaults.list.chroot_install
shim-signed
grub-efi-arm64
grub-efi-arm64-bin
konsole
neon-desktop
EOF
