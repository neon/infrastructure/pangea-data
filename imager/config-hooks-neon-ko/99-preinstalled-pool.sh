#!/bin/sh
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex
echo "config-hooks-neon -- 99-preinstalled-pool"

mkdir -vp -m 0700 config/gnupg
mkdir -vp -m 0700 config/indices

# Make sure we use a suitably strong digest algorithm. SHA1 is deprecated and
# makes apt angry.
cat > config/gnupg/gpg.conf <<EOF
personal-digest-preferences SHA512
cert-digest-algo SHA512
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
EOF

# confirm the conf file
cat config/gnupg/gpg.conf
# make sure we have a copy of the neon-archive-keyring.gpg
# to be used as the preinstalled-pool's pubring.gpg
#wget https://archive.neon.kde.org/public.key
#gpg --home config/gnupg --import public.key
#cp -v public.key config/gnupg/pubring.gpg
#ls -lsh config/gnupg/
#cat config/gnupg/pubring.gpg

for component in $COMPONENTS; do
   (cd config/indices && \
    wget http://archive.ubuntu.com/ubuntu/indices/override.$SUITE.$component && \
    wget http://archive.ubuntu.com/ubuntu/indices/override.$SUITE.extra.$component \
   )
done
