#!/bin/sh
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

set -ex
echo "config-hooks-neon -- **-repo"

if [ "$VERSION_CODENAME" = "jammy" ]; then
apt install -y dirmngr gnupg1
ARGS="--batch --verbose"
GPG="gpg1"

apt-key export '444D ABCF 3667 D028 3F89 4EDD E6D4 7362 5575 1E5D' | $GPG \
  $ARGS \
  --no-default-keyring \
  --primary-keyring config/archives/ubuntu-defaults.key \
  --import

  # make sure _apt can read this file. it may get copied into the chroot
  chmod 644 config/archives/ubuntu-defaults.key || true

  echo "deb http://archive.neon.kde.org/${NEONARCHIVE} $SUITE main" >> config/archives/neon.list
  echo "deb-src http://archive.neon.kde.org/${NEONARCHIVE} $SUITE main" >> config/archives/neon.list

else

  # use ubuntu-defaults.key for iso creation until we can use neon-keyring package for LB_BOOTSTRAP_KEYRING='neon-keyring'
  cp /etc/apt/keyrings/neon-archive-keyring.gpg config/archives/ubuntu-defaults.key
  chmod 644 config/archives/ubuntu-defaults.key || true

  # ensure our non-armoured (binary) public key is copied into the chroot as expected
  mkdir  -p chroot/etc/apt/keyrings/
  cp /etc/apt/keyrings/* chroot/etc/apt/keyrings/

  # make sure _apt can read this file. it will get copied into the chroot
  chmod 644 chroot/etc/apt/keyrings/neon-archive-keyring.asc || true
  chmod 644 chroot/etc/apt/keyrings/neon-archive-keyring.gpg || true

  if [ "$ARCH" = "amd64" ]; then
      ARCH="amd64 i386"
  fi
  # use a signed neon.list until we work out how to enable neon.sources in livecd-rootfs
  cat <<EOF >config/archives/neon.list
deb [signed-by=/etc/apt/keyrings/neon-archive-keyring.asc] http://archive.neon.kde.org/${NEONARCHIVE} $SUITE main
deb-src [signed-by=/etc/apt/keyrings/neon-archive-keyring.asc] http://archive.neon.kde.org/${NEONARCHIVE} $SUITE main
EOF

  # add a nice depreciation message when we actually have depreciated the above hack
  #echo "# KDE neon sources have moved to /etc/apt/sources.list.d/neon.sources" >> config/archives/neon.list

  # add our spiffy new deb822 .sources config
  cat > config/archives/neon.sources << EOF
X-Repolib-Name: KDE neon $SUITE ${NEONARCHIVE}
Types: deb deb-src
URIs: http://archive.neon.kde.org/${NEONARCHIVE}
Suites: $SUITE
Components: main
Signed-By: /etc/apt/keyrings/neon-archive-keyring-pubkey.asc
EOF

  # check our stuffs are in the right place
  ls -lshR config/archives/
  cat config/archives/neon.list
  cat config/archives/neon.sources

fi
